import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isLoginError : boolean = false;
  constructor(private userService : AuthService,private router : Router) { }

  ngOnInit() {
    this.userService.isLoggedIn = false;
  }

  OnSubmit(userName,password){
     this.userService.userAuthentication(userName,password).subscribe((data : any)=>{
       console.log(data);
      localStorage.setItem('userToken',data.access_token);
      localStorage.setItem('userRoles',data.role);
      this.router.navigate(['/home']);
      this.userService.isLoggedIn = true;
    },
    (err : HttpErrorResponse)=>{
      this.isLoginError = true;
      this.userService.isLoggedIn = false;
    });

   
  }

}

