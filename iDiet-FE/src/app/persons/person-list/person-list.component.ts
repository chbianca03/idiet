import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PersonService } from 'src/app/shared/services/person.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Person } from 'src/app/shared/models/data.models';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css']
})
export class PersonListComponent implements OnInit {
  @ViewChild('alert') alert:ElementRef;

list: Person[];
forAdminRole: any;
deleted:boolean=false;
deletedUser: any = null;

  constructor(private service: PersonService, private userService: AuthService,
    private toastr: ToastrService) { }

  ngOnInit() {
   this.service.refreshList();
   this.alert.nativeElement.classList.remove('show');

  }

  populateForm(person: Person) {
    this.service.formData = Object.assign({}, person);
  }

  onDelete(id: number) {
    this.service.list.forEach(user=>{
      if(user.id === id)
        this.deletedUser = user;
    });


    if (confirm('Are you sure to delete this record?')) {
      
      this.service.deletePerson(id).subscribe(res => {
        this.service.refreshList();
       this.alert.nativeElement.classList.add('show');
      });

      //  setTimeout(function(){
      //   this.alert.nativeElement.classList.remove('show');
      //  }, 3000);

    }

  }

  closeAlert(){
    this.alert.nativeElement.classList.remove('show');
  }

}
