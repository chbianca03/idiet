import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PersonService } from 'src/app/shared/services/person.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {
  @ViewChild('alert') alert:ElementRef;
  update:boolean=false;
  wrong:boolean=false;

  constructor(private service: PersonService,
    private toastr: ToastrService) { }


  ngOnInit() {
    this.resetForm();
    this.alert.nativeElement.classList.remove('show');
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.formData = {
      id: null,
      firstName: '',
      lastName: '',
      username: '',
      phoneNumber: '',
      role: '',
      bodyInformation: '',
      email: '',
    }
  }


  onSubmit(form: NgForm) {
    if(!form.value.id){
      this.wrong = true;
      this.update = false;
      this.alert.nativeElement.classList.add('show');
    }
    else {
    this.service.putPerson(form.value).subscribe(res => {
     // this.toastr.info('Updated successfully', 'Person Register');
     this.update = true;
     this.wrong = false;
     this.alert.nativeElement.classList.add('show');
      this.resetForm(form);
      this.service.refreshList();
    });
  }
  
  }

  closeAlert(){
    this.alert.nativeElement.classList.remove('show');
  }

}
