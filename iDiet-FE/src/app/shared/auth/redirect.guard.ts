import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable()
export class RedirectGuard implements CanActivate {
  constructor(private router : Router){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):  boolean {
      console.log(localStorage.getItem('userToken'));
      if (localStorage.getItem('userToken') != null)
      {
        this.router.navigate(['/home']);
        return false;
      }
      //  this.router.navigate(['/login']);
         return true;
      }
      
     // return false;
  
}
