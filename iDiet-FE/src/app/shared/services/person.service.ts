import { Injectable } from '@angular/core';
import { Person } from '../models/data.models';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PersonService {

  formData  : Person;
  list : Person[];
  readonly rootURL ="http://localhost:35257"

  constructor(private http : HttpClient) { }

  postPerson(formData : Person){
    var reqHeader = new HttpHeaders({'No-Auth':'True'});
   return this.http.post(this.rootURL+'/api/PersonModel',formData,{headers : reqHeader});
    
  }

   refreshList(){
    this.http.get(this.rootURL+'/api/GetPersons')
     .toPromise().then(res => this.list = res as Person[]);
   }

  putPerson(formData : Person){
    var reqHeader = new HttpHeaders({'No-Auth':'True'});
    return this.http.put(this.rootURL+'/api/PersonModel/'+ formData.id,formData, {headers : reqHeader});
     
   }

   deletePerson(id : number){
    return this.http.delete(this.rootURL+'/api/PersonModel/'+id);
   }

   getPersonByEmail(email : string){
    var reqHeader = new HttpHeaders({'No-Auth':'True'});
    return this.http.get(this.rootURL+'/api/PersonModel/' + email, {headers : reqHeader}); //"ww@www.www"
   }
}
