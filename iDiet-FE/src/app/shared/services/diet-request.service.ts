import { Injectable } from '@angular/core';
import { DietRequest } from '../models/data.models';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DietRequestService {

  formData  : DietRequest;
  list : DietRequest[];
  listForCurrentClient : DietRequest[];
  readonly rootURL ="http://localhost:35257/api" //7741    50822

  constructor(private http : HttpClient) { }

  postDietRequest(formData : DietRequest){
    var reqHeader = new HttpHeaders({'No-Auth':'True'});
   return this.http.post(this.rootURL+'/DietRequest/',formData,{headers : reqHeader});
    
  }

   refreshList(){
    this.http.get(this.rootURL+'/GetDietRequests/')
    .toPromise().then(res => this.list = res as DietRequest[]);
   }

  putDietRequest(formData : DietRequest){
    var reqHeader = new HttpHeaders({'No-Auth':'True'});
    return this.http.put(this.rootURL+'/DietRequest/'+formData.id,formData, {headers : reqHeader});
     
   }

   deleteDietRequest(id : number){
    return this.http.delete(this.rootURL+'/DietRequest/'+id);
   }

   refreshListForCurrentClient(id: number){
    this.http.get(this.rootURL+'/GetDietRequestsByClientId/' + id)
     .toPromise().then(res => this.listForCurrentClient = res as DietRequest[]);
   }

   refreshListForCurrentClient2(id: number):Observable<any>{
    return this.http.get(this.rootURL+'/GetDietRequestsByClientId/' + id);
  
   }
}
