import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Food } from '../models/data.models';
@Injectable({
  providedIn: 'root'
})
export class FoodService {
  readonly rootURL ="http://localhost:35257/api"
  data : number[][] = [];
  list : Food[];
  index :number = -1;
  constructor(private http : HttpClient) { }


  setList(){
    this.http.get(this.rootURL+'/GetFoods/')
     .toPromise().then(res => {
      this.list = res as Food[],
      this.list.forEach(food => {
        var array : number[] = [];
        array[0] = food.energy;
        array[1] = food.calories;
        array[2] = food.protein;
        array[3] = food.carbohydrate;
        array[4] = food.fat;
        array[5] = food.fibre;
        this.data[++this.index] = array;
      });
     }

      );
   }
}
