import { Injectable } from '@angular/core';
import { DietPlan } from '../models/data.models';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DietPlanService {

  clientId : number;
  dietRequestMessage : string;
  formData  : DietPlan;
  list : DietPlan[];
  listForCurrentClient : DietPlan[];
  readonly rootURL ="http://localhost:35257/api" //7741    50822

  constructor(private http : HttpClient) { }

  postDietPlan(formData : DietPlan){
    var reqHeader = new HttpHeaders({'No-Auth':'True'});
   return this.http.post(this.rootURL+'/DietPlan/',formData,{headers : reqHeader});
    
  }

   refreshList(){
    this.http.get(this.rootURL+'/GetDietPlans/')
     .toPromise().then(res => this.list = res as DietPlan[]);
   }

  putDietPlan(formData : DietPlan){
    var reqHeader = new HttpHeaders({'No-Auth':'True'});
    return this.http.put(this.rootURL+'/DietPlan/'+formData.id,formData, {headers : reqHeader});
     
   }

   deleteDietPlan(id : number){
    return this.http.delete(this.rootURL+'/DietPlan/'+id);
   }

   refreshListForCurrentClient(id: number){
    this.http.get(this.rootURL+'/GetDietPlanByClientId/' + id)
     .toPromise().then(res => this.listForCurrentClient = res as DietPlan[]);
   }
}
