export class User {
    UserName: string;
    Password: string;
    Email: string;
    FirstName: string;
    LastName: string;
    BodyInformation: string;
}

export class Person {
    id: number;
    firstName: string;
    lastName: string;
    username: string;
    phoneNumber: string;
    role: string;
    bodyInformation: string;
    email: string;
}

export class Food {
    id: number;
    name: string;
    energy: number;
    calories: number;
    protein: number;
    carbohydrate: number;
    fat: number;
    fibre: number;
}

export class DietRequest {
    id: number;
    clientId: number;
    requestMessage: string;
}

export class DietPlan {
    id: number;
    clientId: number;
    diet: string;
}


