import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { FoodService } from 'src/app/shared/services/food.service';

@Component({
    selector: 'app-food',
    templateUrl: './food.component.html',
    styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit {
    foodName = '';
    LineChart = [];
    BarChart = [];
    PieChart = [];

    constructor(private service: FoodService) { }

    ngOnInit() {
        this.service.setList();
        this.refreshCharts(0);
        this.foodName = this.service.list[0].name;
    }

    refreshCharts(i: number) {
        this.refreshLineChart(i);
        this.refreshBarChart(i);
        this.refreshPieChart(i);
        this.foodName = this.service.list[i].name;
    }

    refreshLineChart(i: number) {
        // Line chart:
        this.LineChart = new Chart('lineChart', {
            type: 'line',
            data: {
                labels: ["energy", "calories", "protein", "carbohydrate", "fat", "fibre"],
                datasets: [{
                    label: 'Nutrient Value',
                    data: this.service.data[i],
                    fill: false,
                    lineTension: 0.2,
                    borderColor: "red",
                    borderWidth: 1
                }]
            },
            options: {
                title: {
                    text: "Line Chart",
                    display: true
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }

    refreshBarChart(i: number) {
        // Bar chart:
        this.BarChart = new Chart('barChart', {
            type: 'bar',
            data: {

                labels: ["energy", "calories", "protein", "carbohydrate", "fat", "fibre"],
                datasets: [{
                    label: 'Nutrients Value',
                    data: this.service.data[i],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                title: {
                    text: "Bar Chart",
                    display: true
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }

    refreshPieChart(i: number) {
        // pie chart:
        this.PieChart = new Chart('pieChart', {
            type: 'pie',
            data: {
                labels: ["energy", "calories", "protein", "carbohydrate", "fat", "fibre"],
                datasets: [{
                    label: '# of Votes',
                    data: this.service.data[i],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                title: {
                    text: "Bar Chart",
                    display: true
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }

    goBack() {
        window.history.back();
      }
}