import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-forbidden',
  templateUrl: './forbidden.component.html',
  styleUrls: ['./forbidden.component.scss']
})
export class ForbiddenComponent implements OnInit {

  constructor(private userService:AuthService) { }

  ngOnInit() {
    //localStorage.removeItem('userToken')
    //localStorage.removeItem('userRoles')

    this.userService.isLoggedIn = false;
  }

}
