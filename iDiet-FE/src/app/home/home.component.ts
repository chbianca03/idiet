import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  userClaims: any;
  idPerson: number;

  constructor(private router: Router, private userService: AuthService) { }

  ngOnInit() {
    this.userService.getUserClaims().subscribe((data: any) => {
      this.userClaims = data;

    });

    if(this.router.url === '/home'){
      this.userService.isLoggedIn = true;
    }
  }

  getIdPersonByEmail(){
    this.userService.getIdPersonByEmail(this.userClaims.Email).subscribe((id : number) => {
      this.idPerson = id;
    })
  }




}
