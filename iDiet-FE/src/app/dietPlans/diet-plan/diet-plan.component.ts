import { Component, OnInit } from '@angular/core';
import { DietPlanService } from 'src/app/shared/services/diet-plan.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/shared/services/auth.service';
@Component({
  selector: 'app-diet-plan',
  templateUrl: './diet-plan.component.html',
  styleUrls: ['./diet-plan.component.css']
})
export class DietPlanComponent implements OnInit {

  clientId: number;

  constructor(private service: DietPlanService,  private userService: AuthService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
    this.userService.getUserClaims().subscribe((data: any) => {
      this.clientId = data.IdPerson; });
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.formData = {
      id: null,
      clientId: null,
      diet: '',
    }
  }

  onSubmit(form: NgForm) {
    if (form.value.id == null)
      this.insertRecord(form);
    else
      this.updateRecord(form);
  }

  insertRecord(form: NgForm) {
    form.value.clientId = this.clientId;
    this.service.postDietPlan(form.value).subscribe(res => {
      this.toastr.success('Inserted successfully', 'DietPlan Register');
      this.resetForm(form);
      this.service.refreshList();
    });
  }

  updateRecord(form: NgForm) {
    form.value.clientId = this.clientId;
    this.service.putDietPlan(form.value).subscribe(res => {
      this.toastr.info('Updated successfully', 'DietPlan Register');
      this.resetForm(form);
      this.service.refreshList();
    });
  }
}
