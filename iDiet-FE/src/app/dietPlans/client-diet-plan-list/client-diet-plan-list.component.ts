import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DietPlanService } from 'src/app/shared/services/diet-plan.service';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-client-diet-plan-list',
  templateUrl: './client-diet-plan-list.component.html',
  styleUrls: ['./client-diet-plan-list.component.css']
})
export class ClientDietPlanListComponent implements OnInit {

  clientId: number;

  constructor(private service: DietPlanService, private userService: AuthService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.userService.getUserClaims().subscribe((data: any) => {
      this.clientId = data.IdPerson;     
      this.service.refreshListForCurrentClient(this.clientId);
    });
  }

  goBack() {
    window.history.back();
  }


}
