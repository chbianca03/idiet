import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientDietPlanListComponent } from './client-diet-plan-list.component';

describe('ClientDietPlanListComponent', () => {
  let component: ClientDietPlanListComponent;
  let fixture: ComponentFixture<ClientDietPlanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientDietPlanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientDietPlanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
