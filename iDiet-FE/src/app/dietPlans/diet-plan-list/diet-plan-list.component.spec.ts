import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietPlanListComponent } from './diet-plan-list.component';

describe('DietPlanListComponent', () => {
  let component: DietPlanListComponent;
  let fixture: ComponentFixture<DietPlanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietPlanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietPlanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
