import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DietPlanService } from 'src/app/shared/services/diet-plan.service';
import { DietPlan } from 'src/app/shared/models/data.models';
@Component({
  selector: 'app-diet-plan-list',
  templateUrl: './diet-plan-list.component.html',
  styleUrls: ['./diet-plan-list.component.css']
})
export class DietPlanListComponent implements OnInit {

  constructor(private service: DietPlanService,
    private toastr: ToastrService) { }

    ngOnInit() {    
      this.service.refreshList();
    }
  
    populateForm(dietPlan: DietPlan) {
      this.service.formData = Object.assign({}, dietPlan);
    }
  
    onDelete(id: number) {
      if (confirm('Are you sure to delete this record?')) {
        this.service.deleteDietPlan(id).subscribe(res => {
          this.service.refreshList();;
         this.toastr.warning('Deleted successfully', 'DietPlan Register');
        });
      }
    }
  
  }
  