import { Component, OnInit } from '@angular/core';
import { AuthGuard } from './shared/auth/auth.guard';
import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'iDiet-FE';

  constructor(private userService: AuthService){

  }


  ngOnInit(){
    
  }
}
