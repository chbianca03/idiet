import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { RegisterComponent } from './user/register/register.component';
import { LoginComponent } from './user/login/login.component';
import { AuthGuard } from './shared/auth/auth.guard';
import { DietPlanForRequestsComponent } from './DietPlanForRequest/diet-plan-for-requests/diet-plan-for-requests.component';
import { ClientDietPlanListComponent } from './dietPlans/client-diet-plan-list/client-diet-plan-list.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { FoodComponent} from './food/food.component';
import { RedirectGuard } from './shared/auth/redirect.guard';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent,canActivate:[AuthGuard] },
  { path: 'forbidden', component: ForbiddenComponent, canActivate: [AuthGuard] },
  { path: 'dieticianPanel', component: DietPlanForRequestsComponent, canActivate: [AuthGuard] , data: { roles: ['Dietician'] }},
  { path: 'clientPanel', component: ClientDietPlanListComponent, canActivate: [AuthGuard] , data: { roles: ['Client'] }},
  { path: 'foodPanel', component: FoodComponent, canActivate: [AuthGuard] , data: { roles: ['Dietician', 'Client', 'Admin'] }},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] , data: { roles: ['Dietician', 'Client', 'Admin'] }},
  {
    path: 'register', component: UserComponent, canActivate:[RedirectGuard],
    children: [{ path: '', component: RegisterComponent }]
  },
  {
    path: 'login', component: UserComponent, canActivate:[RedirectGuard],
    children: [{ path: '', component: LoginComponent }]
  },
  { path : '', redirectTo:'/login', pathMatch : 'full'}

  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { 

}
