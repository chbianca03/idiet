import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './user/login/login.component';
import { RegisterComponent } from './user/register/register.component';
import { UserComponent } from './user/user.component';


import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { AuthInterceptor } from './shared/auth/auth.interceptor';
import { AuthGuard } from './shared/auth/auth.guard';
import { AuthService } from './shared/services/auth.service';
import { DietPlanForRequestComponent } from './DietPlanForRequest/diet-plan-for-request/diet-plan-for-request.component';
import { DietPlanForRequestsComponent } from './DietPlanForRequest/diet-plan-for-requests/diet-plan-for-requests.component';
import { DietPlanForRequestListComponent } from './DietPlanForRequest/diet-plan-for-request-list/diet-plan-for-request-list.component';
import { ClientDietPlanListComponent } from './dietPlans/client-diet-plan-list/client-diet-plan-list.component';
import { DietPlanComponent } from './dietPlans/diet-plan/diet-plan.component';
import { DietPlanListComponent } from './dietPlans/diet-plan-list/diet-plan-list.component';
import { DietPlansComponent } from './dietPlans/diet-plans/diet-plans.component';
import { DietRequestComponent } from './dietRequests/diet-request/diet-request.component';
import { DietRequestListComponent } from './dietRequests/diet-request-list/diet-request-list.component';
import { DietRequestsComponent } from './dietRequests/diet-requests/diet-requests.component';
import { FoodComponent } from './food/food.component';
import { PersonsComponent } from './persons/persons.component';
import { PersonComponent } from './persons/person/person.component';
import { PersonListComponent } from './persons/person-list/person-list.component';
import { HomeComponent } from './home/home.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { RedirectGuard } from './shared/auth/redirect.guard';
import { ProfileComponent } from './profile/profile.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UserComponent,
    DietPlanForRequestComponent,
    DietPlanForRequestsComponent,
    DietPlanForRequestListComponent,
    ClientDietPlanListComponent,
    DietPlanComponent,
    DietPlanListComponent,
    DietPlansComponent,
    DietRequestComponent,
    DietRequestListComponent,
    DietRequestsComponent,
    FoodComponent,
    PersonsComponent,
    PersonComponent,
    PersonListComponent,
    HomeComponent,
    ForbiddenComponent,
    ProfileComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [AuthService,AuthGuard,RedirectGuard,
  {
    provide : HTTP_INTERCEPTORS,
    useClass : AuthInterceptor,
    multi : true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }

