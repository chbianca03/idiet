import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietRequestComponent } from './diet-request.component';

describe('DietRequestComponent', () => {
  let component: DietRequestComponent;
  let fixture: ComponentFixture<DietRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
