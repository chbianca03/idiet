import { Component, OnInit } from '@angular/core';
import { DietRequestService } from 'src/app/shared/services/diet-request.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-diet-request',
  templateUrl: './diet-request.component.html',
  styleUrls: ['./diet-request.component.css']
})
export class DietRequestComponent implements OnInit {

  clientId: number;

  constructor(private service: DietRequestService,private userService: AuthService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
    this.userService.getUserClaims().subscribe((data: any) => {
      this.clientId = data.IdPerson;    });
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.formData = {
      id: null,
      clientId: null,
      requestMessage: '',
    }
  }


  onSubmit(form: NgForm) {
    if (form.value.id == null)
      this.insertRecord(form);
    else
      this.updateRecord(form);
  }

  insertRecord(form: NgForm) {
    form.value.clientId = this.clientId;
    this.service.postDietRequest(form.value).subscribe(res => {
      this.toastr.success('Inserted successfully', 'DietRequest Register');
      this.resetForm(form);
      this.service.refreshListForCurrentClient(this.clientId);
      // this.service.refreshListForCurrentClient2(this.clientId).subscribe(item => {
      // })
    });
  }

  updateRecord(form: NgForm) {
    form.value.clientId = this.clientId;
    this.service.putDietRequest(form.value).subscribe(res => {
      this.toastr.info('Updated successfully', 'DietRequest Register');
      this.resetForm(form);
      this.service.refreshListForCurrentClient(this.clientId);
    });

  }

}
