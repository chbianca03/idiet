import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietRequestListComponent } from './diet-request-list.component';

describe('DietRequestListComponent', () => {
  let component: DietRequestListComponent;
  let fixture: ComponentFixture<DietRequestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietRequestListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
