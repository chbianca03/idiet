import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DietRequestService } from 'src/app/shared/services/diet-request.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DietRequest } from '../../shared/models/data.models';

@Component({
  selector: 'app-diet-request-list',
  templateUrl: './diet-request-list.component.html',
  styleUrls: ['./diet-request-list.component.css']
})
export class DietRequestListComponent implements OnInit {

  clientId: number;

  constructor(private service: DietRequestService, private userService: AuthService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.userService.getUserClaims().subscribe((data: any) => {
      this.clientId = data.IdPerson;
      this.service.refreshListForCurrentClient(this.clientId);
    });
  }

  populateForm(dietRequest: DietRequest) {
    this.service.formData = Object.assign({}, dietRequest);
  }

  onDelete(id: number) {
    if (confirm('Are you sure to delete this record?')) {
      this.service.deleteDietRequest(id).subscribe(res => {
        this.service.refreshListForCurrentClient(this.clientId);
        this.toastr.warning('Deleted successfully', 'DietRequest Register');
      });
    }
  }

}
