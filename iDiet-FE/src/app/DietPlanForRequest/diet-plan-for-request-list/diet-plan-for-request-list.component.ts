import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DietRequestService } from 'src/app/shared/services/diet-request.service';
import { DietPlanService } from 'src/app/shared/services/diet-plan.service';
import { DietRequest } from 'src/app/shared/models/data.models';


@Component({
  selector: 'app-diet-plan-for-request-list',
  templateUrl: './diet-plan-for-request-list.component.html',
  styleUrls: ['./diet-plan-for-request-list.component.css']
})
export class DietPlanForRequestListComponent implements OnInit {

  constructor(private service: DietRequestService, private dietPlanService: DietPlanService,
    private toastr: ToastrService) { }

    ngOnInit() {    
      this.service.refreshList();
    }

  populateForm(dietRequest: DietRequest) {
    this.dietPlanService.dietRequestMessage = dietRequest.requestMessage;
    this.dietPlanService.clientId = dietRequest.clientId;
  }

}