import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietPlanForRequestListComponent } from './diet-plan-for-request-list.component';

describe('DietPlanForRequestListComponent', () => {
  let component: DietPlanForRequestListComponent;
  let fixture: ComponentFixture<DietPlanForRequestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietPlanForRequestListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietPlanForRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
