import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-diet-plan-for-requests',
  templateUrl: './diet-plan-for-requests.component.html',
  styleUrls: ['./diet-plan-for-requests.component.css']
})
export class DietPlanForRequestsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  goBack() {
    window.history.back();
  }
}
