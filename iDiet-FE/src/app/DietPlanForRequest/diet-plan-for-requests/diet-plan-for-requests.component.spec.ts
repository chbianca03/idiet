import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietPlanForRequestsComponent } from './diet-plan-for-requests.component';

describe('DietPlanForRequestsComponent', () => {
  let component: DietPlanForRequestsComponent;
  let fixture: ComponentFixture<DietPlanForRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietPlanForRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietPlanForRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
