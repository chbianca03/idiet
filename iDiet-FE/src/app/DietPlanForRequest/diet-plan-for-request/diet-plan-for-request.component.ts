import { Component, OnInit } from '@angular/core';
import { DietRequestService } from 'src/app/shared/services/diet-request.service';
import { DietPlanService } from 'src/app/shared/services/diet-plan.service'
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-diet-plan-for-request',
  templateUrl: './diet-plan-for-request.component.html',
  styleUrls: ['./diet-plan-for-request.component.css']
})
export class DietPlanForRequestComponent implements OnInit {

  constructor(private service: DietPlanService,private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.dietRequestMessage = '';
    this.service.formData = {
      id: null,
      clientId: null,
      diet: '',
    }
  }
  
  onSubmit(form: NgForm) {
    form.value.clientId = this.service.clientId;
    this.service.postDietPlan(form.value).subscribe(res => {
      this.toastr.success('Inserted successfully', 'DietPlan Register');
      this.resetForm(form);
      this.service.refreshList();
    });
  }

}
