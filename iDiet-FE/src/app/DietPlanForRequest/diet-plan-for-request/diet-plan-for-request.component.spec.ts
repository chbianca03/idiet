import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietPlanForRequestComponent } from './diet-plan-for-request.component';

describe('DietPlanForRequestComponent', () => {
  let component: DietPlanForRequestComponent;
  let fixture: ComponentFixture<DietPlanForRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietPlanForRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietPlanForRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
