import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { PersonService } from '../shared/services/person.service';
import { NgForm } from '@angular/forms';
import { User, Person } from '../shared/models/data.models';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userClaims: any;
  update:boolean=false;
  wrong:boolean=false;
  userUpdate: Person;
  @ViewChild('alert') alert:ElementRef;
  
  constructor(private userService: AuthService, private service: PersonService) { }

  ngOnInit() {
    this.userService.getUserClaims().subscribe((data: any) => {
      console.log(data);
      this.userClaims = data;
      this.userUpdate = data;
     
      this.userUpdate = {
        id : data.IdPerson,
        firstName : data.FirstName,
        lastName : data.LastName,
        username : data.UserName,
        phoneNumber: null,
        bodyInformation:null,
        role: null,
        email: data.Email
      }

      console.log(this.userUpdate);
    });

    // this.resetForm();
    this.alert.nativeElement.classList.remove('show');
  }


  onSubmit(form: NgForm) {
    if(!form.value.id){
      this.wrong = true;
      this.update = false;
      this.alert.nativeElement.classList.add('show');
    }
    else {
      console.log(form.value);
    this.service.putPerson(form.value).subscribe(res => {
     // this.toastr.info('Updated successfully', 'Person Register');
     this.update = true;
     this.wrong = false;
     this.alert.nativeElement.classList.add('show');
     // this.resetForm(form);
      this.service.refreshList();
    });

    this.userUpdate = form.value;
  }
  
  }

  closeAlert(){
    this.alert.nativeElement.classList.remove('show');
  }

  goBack() {
    window.history.back();
  }

}
