import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private userService: AuthService, private router:Router) { }

  ngOnInit() {

  }

  Logout() {
    localStorage.removeItem('userToken');
    localStorage.removeItem('userRoles');
    this.userService.isLoggedIn = false;
    this.router.navigate(['/login']);
  }

}
