using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class DietRequestController : ApiController
    {
        private DietConsultantDBEntities db = new DietConsultantDBEntities();

        // GET: api/DietRequest
        [HttpGet]
        [Route("api/GetDietRequests")]
        [AllowAnonymous]
        public HttpResponseMessage GetDietRequests()
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, db.dietRequests);
        }

        [HttpGet]
        [Route("api/GetDietRequestsByClientId/{id}")]
        [AllowAnonymous]
        public HttpResponseMessage GetDietRequestsByClientId(int id)
        {
            List<dietRequest> list = new List<dietRequest>();
            foreach(dietRequest diet in db.dietRequests)
            {
                if (diet.clientId == id)
                    list.Add(diet);
            }

            return this.Request.CreateResponse(HttpStatusCode.OK, list);
        }

        // GET: api/DietRequest/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(dietRequest))]
        public IHttpActionResult GetdietRequest(int id)
        {
            dietRequest dietRequest = db.dietRequests.Find(id);
            if (dietRequest == null)
            {
                return NotFound();
            }

            return Ok(dietRequest);
        }

        // PUT: api/DietRequest/5
        [HttpPut]
        [AllowAnonymous]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutdietRequest(int id, dietRequest dietRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dietRequest.id)
            {
                return BadRequest();
            }

            db.Entry(dietRequest).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!dietRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DietRequest
        [HttpPost]
        [AllowAnonymous]
        [ResponseType(typeof(dietRequest))]
        public IHttpActionResult PostdietRequest(dietRequest dietRequest)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            db.dietRequests.Add(dietRequest);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = dietRequest.id }, dietRequest);
        }

        // DELETE: api/DietRequest/5
        [Authorize(Roles = "Client")]
        [ResponseType(typeof(dietRequest))]
        public IHttpActionResult DeletedietRequest(int id)
        {
            dietRequest dietRequest = db.dietRequests.Find(id);
            if (dietRequest == null)
            {
                return NotFound();
            }

            db.dietRequests.Remove(dietRequest);
            db.SaveChanges();

            return Ok(dietRequest);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool dietRequestExists(int id)
        {
            return db.dietRequests.Count(e => e.id == id) > 0;
        }
    }
}