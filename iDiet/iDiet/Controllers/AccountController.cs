﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class AccountController : ApiController
    {
        private DietConsultantDBEntities db = new DietConsultantDBEntities();


        [Route("api/User/Register")]
        [HttpPost]
        [AllowAnonymous]
        public IdentityResult Register(AccountModel model)
        {
            int idPerson = RegisterInDBEntities(model);
            var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(userStore);
            var user = new ApplicationUser()
            {
                UserName = model.UserName,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                IdPerson = idPerson

            };
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 3
            };
            IdentityResult result = manager.Create(user, model.Password);
            manager.AddToRoles(user.Id, model.Roles);
            return result;
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Client, Dietician")]
        [Route("api/GetIdPersonByEmail")]
        public int GetIdPersonByEmail(string email)
        {
            var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(userStore);
            return manager.FindByEmail(email).IdPerson;
        }

        [HttpGet]
        [Route("api/GetUserClaims")]
        public AccountModel GetUserClaims()
        {
            var identityClaims = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identityClaims.Claims;
            AccountModel model = new AccountModel()
            {
                UserName = identityClaims.FindFirst("Username").Value,
                Email = identityClaims.FindFirst("Email").Value,
                FirstName = identityClaims.FindFirst("FirstName").Value,
                LastName = identityClaims.FindFirst("LastName").Value,
                IdPerson = int.Parse(identityClaims.FindFirst("IdPerson").Value),
                LoggedOn = identityClaims.FindFirst("LoggedOn").Value
            };
            return model;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("api/ForAdminRole")]
        public List<Person> ForAdminRole()
        {
            List<Person> list = new List<Person>();
            foreach (var p in db.personModels)
            {
                list.Add(new Person { firstName = p.firstName });
            }
            return list;
        }

        private int RegisterInDBEntities(AccountModel model)
        {
            //Entity Person Model correlated to new User 
            var personModel = db.personModels.Add(new personModel()
            {
                firstName = model.FirstName,
                lastName = model.LastName,
                username = model.UserName,
                password = model.Password,
                email = model.Email,
                role = model.Roles[0],
                bodyInformation = model.BodyInformation
            });
            db.SaveChanges();
            return personModel.id;
        }
    }
}
