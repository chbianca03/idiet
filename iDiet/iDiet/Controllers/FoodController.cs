using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class FoodController : ApiController
    {
        private DietConsultantDBEntities db = new DietConsultantDBEntities();

        // GET: api/Food
        [HttpGet]
        [Route("api/GetFoods")]
        [AllowAnonymous]
        public HttpResponseMessage GetFoods()
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, db.foods);
        }

        // GET: api/Food/5
        [ResponseType(typeof(food))]
        public IHttpActionResult Getfood(int id)
        {
            food food = db.foods.Find(id);
            if (food == null)
            {
                return NotFound();
            }

            return Ok(food);
        }

        // PUT: api/Food/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putfood(int id, food food)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != food.id)
            {
                return BadRequest();
            }

            db.Entry(food).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!foodExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Food
        [ResponseType(typeof(food))]
        public IHttpActionResult Postfood(food food)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.foods.Add(food);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = food.id }, food);
        }

        // DELETE: api/Food/5
        [ResponseType(typeof(food))]
        public IHttpActionResult Deletefood(int id)
        {
            food food = db.foods.Find(id);
            if (food == null)
            {
                return NotFound();
            }

            db.foods.Remove(food);
            db.SaveChanges();

            return Ok(food);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool foodExists(int id)
        {
            return db.foods.Count(e => e.id == id) > 0;
        }
    }
}