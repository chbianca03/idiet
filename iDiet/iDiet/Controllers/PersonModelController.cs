using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WebAPI.Controllers
{
    public class PersonModelController : ApiController
    {
        private DietConsultantDBEntities db = new DietConsultantDBEntities();

        [HttpGet]
        [Route("api/GetPersons")]
        [AllowAnonymous]
        public HttpResponseMessage GetPersons()
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, db.personModels);
        }

        // GET: api/PersonModel/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(personModel))]
        public IHttpActionResult GetpersonModel(int id)
        {
            personModel personModel = db.personModels.Find(id);
            if (personModel == null)
            {
                return NotFound();
            }

            return Ok(personModel);
        }

        // PUT: api/PersonModel/5
        [HttpPut]  
        [AllowAnonymous]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutpersonModel(int id, personModel personModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != personModel.id)
            {
                return BadRequest();
            }

            db.Entry(personModel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!personModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            UpdateUser(personModel);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PersonModel
        [HttpPost]
        [AllowAnonymous]
        [ResponseType(typeof(personModel))]
        public IHttpActionResult PostpersonModel(personModel personModel)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}
            db.personModels.Add(personModel);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = personModel.id }, personModel);
        }

        // DELETE: api/PersonModel/5
        [Authorize(Roles = "Admin")]
   //     [Route("api/ForAdminRole")]
        [ResponseType(typeof(personModel))]
        public IHttpActionResult DeletepersonModel(int id)
        {
            personModel personModel = db.personModels.Find(id);
            if (personModel == null)
            {
                return NotFound();
            }

            db.personModels.Remove(personModel);
            db.SaveChanges();
            DeleteUser(personModel.email);

            return Ok(personModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool personModelExists(int id)
        {
            return db.personModels.Count(e => e.id == id) > 0;
        }

        public void UpdateUser(personModel personModel)
        {
            var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(userStore);
            var user = manager.FindByEmail(personModel.email);
            user.FirstName = personModel.firstName;
            user.LastName = personModel.lastName;
            user.UserName = personModel.username;
            manager.Update(user);
        }

        public void DeleteUser(string email)
        {
            var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(userStore);
            var user = manager.FindByEmail(email);
            manager.Delete(user);
        }

        //// GET: api/PersonModel
        //[HttpGet]
        //[Authorize(Roles = "Admin")]
        //[Route("api/GetpersonModels")]
        //public IQueryable<personModel> GetpersonModels()
        //{
        //    return db.personModels;
        //}
    }
}