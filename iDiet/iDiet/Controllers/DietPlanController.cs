using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class DietPlanController : ApiController
    {
        private DietConsultantDBEntities db = new DietConsultantDBEntities();

        // GET: api/DietPlan
        [HttpGet]
        [Route("api/GetDietPlans")]
        [AllowAnonymous]
        public HttpResponseMessage GetDietPlans()
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, db.dietPlans);
        }

        [HttpGet]
        [Route("api/GetDietPlanByClientId/{id}")]
        [AllowAnonymous]
        public HttpResponseMessage GetDietPlanByClientId(int id)
        {
            List<dietPlan> list = new List<dietPlan>();
            foreach (dietPlan diet in db.dietPlans)
            {
                if (diet.clientId == id)
                    list.Add(diet);
            }

            return this.Request.CreateResponse(HttpStatusCode.OK, list);
        }

        // GET: api/DietPlan/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(dietPlan))]
        public IHttpActionResult GetdietPlan(int id)
        {
            dietPlan dietPlan = db.dietPlans.Find(id);
            if (dietPlan == null)
            {
                return NotFound();
            }

            return Ok(dietPlan);
        }

        // PUT: api/DietPlan/5
        [HttpPut] 
        [AllowAnonymous]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutdietPlan(int id, dietPlan dietPlan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dietPlan.id)
            {
                return BadRequest();
            }

            db.Entry(dietPlan).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!dietPlanExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DietPlan
        [HttpPost]
        [AllowAnonymous]
        [ResponseType(typeof(dietPlan))]
        public IHttpActionResult PostdietPlan(dietPlan dietPlan)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}
            db.dietPlans.Add(dietPlan);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = dietPlan.id }, dietPlan);
        }

        // DELETE: api/DietPlan/5
        [Authorize(Roles = "Dietician")]
        [ResponseType(typeof(dietPlan))]
        public IHttpActionResult DeletedietPlan(int id)
        {
            dietPlan dietPlan = db.dietPlans.Find(id);
            if (dietPlan == null)
            {
                return NotFound();
            }

            db.dietPlans.Remove(dietPlan);
            db.SaveChanges();

            return Ok(dietPlan);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool dietPlanExists(int id)
        {
            return db.dietPlans.Count(e => e.id == id) > 0;
        }
    }
}